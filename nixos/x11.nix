{ config, pkgs, ... }:
{
  services = {
    xserver = {
      enable = true;
      displayManager.autoLogin = {
        enable = true;
        user = config.chasecaleb.username;
      };
      serverFlagsSection = ''
        Option "BlankTime"   "0"
        Option "StandbyTime" "0"
        Option "SuspendTime" "0"
        Option "OffTime"     "0"
      '';
      # Use caps lock as control because I don't want Emacs pinky.
      xkbOptions = "ctrl:nocaps";
    };
    xbanish = {
      enable = true;
      arguments = "-m nw";
    };
  };
}
