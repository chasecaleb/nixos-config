{ config, modulesPath, ... }: {
  imports = [ (modulesPath + "/profiles/qemu-guest.nix") ];

  config = {
    # Manual step for: add a filesystem in virt-manager with this target path. Make sure to change
    # "mode" to squash, otherwise permissions will get messed up.
    fileSystems."/home/${config.chasecaleb.username}/code" = {
      device = "/home/${config.chasecaleb.username}/code";
      fsType = "9p";
      options = [ "trans=virtio" "version=9p2000.L" ];
    };
    services.spice-vdagentd.enable = true;
    services.qemuGuest.enable = true;
  };
}
