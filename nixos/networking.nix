{ config, pkgs, ... }:
{
  services.unbound = {
    enable = true;
    resolveLocalQueries = true; # Register unbound in /etc/resolv.conf
    settings = {
      server.prefetch = true;
      forward-zone = [
        {
          name = ".";
          forward-tls-upstream = true;
          forward-addr = [
            "9.9.9.9@853#dns.quad9.net"
            "149.112.112.112@853#dns.quad9.net"
            "1.1.1.1@853#cloudflare-dns.com"
            "1.0.0.1@853#cloudflare-dns.com"
          ];
        }
      ] ++
      # Work VPN does split routing based on DNS, so I need to use DNS server configured on my
      # Mac (VM host) for any traffic that I want to go through the VPN regardless of whether or
      # not the domain is publically resolvable.
      (pkgs.lib.lists.optionals config.chasecaleb.workFeatures
        (map
          (name: {
            inherit name;
            forward-tls-upstream = false;
            # forward-addrs are the VPN's DNS servers, determined from running on host:
            # scutil --dns | grep nameserver
            forward-addr = [ "172.22.96.101" "172.23.96.101" ];
          })
          [ "nipr.com" "nipr.io" "naic.org" "naiccorp.net" "amazonaws.com" ]));
    };
  };
}
