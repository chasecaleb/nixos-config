{ config, lib, pkgs, myPkgs, myEmacsPkgs, ... }:
{
  nix = {
    # extraOptions is for nix.conf. See man nix.conf.
    extraOptions = ''
      experimental-features = nix-command flakes
      # I know how to use git, I don't need constant dirty warnings.
      warn-dirty = false
    '';
    # Automatically hardlink identical files.
    optimise.automatic = true;
    # Garbage collection on timer, because I don't have infinite storage.
    gc.automatic = true;
    gc.options = "--delete-older-than 14d";
  };
  nixpkgs.config.allowUnfree = true;

  boot = {
    initrd.luks.devices.system.device = "/dev/disk/by-partlabel/cryptsystem";
    # https://nixos.wiki/wiki/Linux_kernel
    kernelPackages = pkgs.linuxPackages_zen;
    kernel.sysctl = {
      # sysctl keys have to be quoted so that nix doesn't treat them as nested attributes.
      "fs.inotify.max_user_watches" = 524288;
      "net.ipv4.ip_forward" = 1;
      "net.ipv6.conf.default.forwarding" = 1;
      "net.ipv6.conf.all.forwarding" = 1;
      "vm.swappiness" = 1;
      "vm.vfs_cache_pressure" = 50;
      "vm.dirty_background_ratio" = 5;
      "vm.dirty_ratio" = 10;
      "vm.min_free_kbytes" = 135168;
    };
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    tmpOnTmpfs = true;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/system";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-label/EFI";
      fsType = "vfat";
    };
  };

  hardware = {
    enableRedistributableFirmware = true;
    cpu.amd.updateMicrocode = true;
    cpu.intel.updateMicrocode = true;
  };

  # I use zram swap instead of a swap partition or file. Downside is that I can't use hibernation
  # (aka suspend-to-disk), but most of my machines have so much RAM that hibernation takes longer
  # than a restart so it isn't useful to me.
  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

  documentation.man = {
    # Support apropros and man -k, which I use for consult-man.
    # https://nixos.wiki/wiki/Apropos
    generateCaches = true;
    # Exclude my Emacs config, because it doesn't have any man pages and rebuilding cache on every
    # config change is slow.
    man-db.manualPages = pkgs.buildEnv {
      name = "man-paths";
      paths =
        let
          emacsPkgs = builtins.attrValues myEmacsPkgs;
        in
        builtins.filter
          (p: !(builtins.elem p emacsPkgs))
          config.environment.systemPackages;
      pathsToLink = [ "/share/man" ];
      extraOutputsToInstall = [ "man" ]
        ++ lib.optionals config.documentation.dev.enable [ "devman" ];
      ignoreCollisions = true;
    };
  };

  environment = {
    # Make zsh completions work for system packages.
    pathsToLink = [ "/share/zsh" ];
    systemPackages = myPkgs ++ (with pkgs; [
      awscli2
      dig
      fd
      git
      git-sync # Used to auto commit/push/pull some of my git repos (e.g. org-files).
      go
      gopls
      htop
      i3lock
      inotify-tools
      jq
      kubectl
      kubie
      less
      libnotify
      lsof
      mtr
      ncdu
      nix-diff
      nmap
      nodePackages.bash-language-server
      nodePackages.typescript # Needed for lsp-mode (just the language server isn't enough).
      nodePackages.typescript-language-server
      nodePackages.vscode-json-languageserver
      parallel
      pass
      pavucontrol
      rnix-lsp
      scc # Lines-of-code counter (plus complexity calcuations and more)
      shellcheck
      silver-searcher # ag
      ssm-session-manager-plugin # Session manager (SSO) for AWS CLI
      terraform-lsp
      traceroute
      virt-manager
      wget
      whois
      yq
    ]);
  };

  fonts = {
    # Defaults listed here (including some for unicode coverage):
    # https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/config/fonts/fonts.nix
    enableDefaultFonts = true;
    # Ghostscript fonts include Acrobat Reader's "Base 14" (Courier, Helvetica, and Times Roman
    # familes) plus... some others. TLDR I think these are important for compatibility in certain
    # programs like PDF readers.
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      hack-font
      material-icons # For polybar
      emacs-all-the-icons-fonts # Material Design, all-the-icons, GitHub octicons, and a few others.
    ];
    fontconfig.defaultFonts.monospace = [ "Hack" ];
  };

  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark; # I want the GUI, not just CLI.
  };
  # zsh.enable is necessary in system config for completions to work, even when also enabled in
  # home-manager.
  programs.zsh.enable = true;

  services.logind.extraConfig = ''
    HandleLidSwitch=ignore
    HandlePowerKey=ignore
    HandleSuspendKey=ignore
    HandleHibernateKey=ignore
    # Kill user processes (including systemd services) on logout.
    KillUserProcesses=yes
  '';

  hardware.pulseaudio.enable = true;

  security.sudo = {
    # Prevent non-wheel users from executing sudo for security.
    execWheelOnly = true;
    extraConfig = ''
      Defaults timestamp_timeout=30
      # passwd_timeout=0 migrated from my Arch Linux config... but I can't remember why I set it.
      # Seems like tramp had something to do with it.
      Defaults passwd_timeout=0
    '';
  };

  system.stateVersion = "21.11"; # Don't change stateVersion.

  # Hey systemd-boot, reinventing the wheel with your own method of setting the default boot entry
  # (by pressing "d" when on the list) is dumb. There's already a default specified in
  # /boot/loader/loader.conf.
  # https://github.com/systemd/systemd/issues/15068
  systemd.services.clear-systemd-boot-default = {
    description = "Clear systemd-boot's default entry";
    wantedBy = [ "default.target" ];
    script = ''${pkgs.systemd}/bin/bootctl set-default ""'';
  };

  time.timeZone = "US/Central";

  users = {
    mutableUsers = false;
    users.root = {
      # nix-shell -p mkpasswd --run 'mkpasswd -m sha-512'
      hashedPassword = "$6$MlxUZ8gB5PQltXbI$15F56Pr0g/u5QLH/FZQmKM/YjDRO.fNoOS4QrCyLYwmWams4OzPWCgdETz2C4Ya6xqa6yNrCrt6vG4mdm4UM71";
    };
    users.${config.chasecaleb.username} = {
      isNormalUser = true;
      home = "/home/${config.chasecaleb.username}";
      extraGroups = [ "libvirtd" "wheel" ];
      # nix-shell -p mkpasswd --run 'mkpasswd -m sha-512'
      hashedPassword = "$6$/2Nf.4hy9xd/lnlZ$PxZY53nuCKIRRIDhPUolY9GN/6WCwvaj9fv/8OiUxt9Hx0VbGtv66IEdEccYknWtTPXiPz7S2OcICbudZgZg1.";
      shell = pkgs.zsh;
    };
  };

  sops = {
    age.keyFile = "/var/lib/private/sops.key";
    defaultSopsFile = ../secrets/main.yaml;
    secrets = {
      nix-cache-access-key = { };
      "ssh/gitlab-chasecaleb" = {
        owner = config.chasecaleb.username;
      };
      "ssh/github" = {
        owner = config.chasecaleb.username;
      };
    } // (if config.chasecaleb.workFeatures then {
      "ssh/gitlab-work" = {
        owner = config.chasecaleb.username;
      };
      "aws-config/work" = {
        owner = config.chasecaleb.username;
        path = "/home/${config.chasecaleb.username}/.aws/config";
      };
    } else {
      "ssh/octopi" = {
        owner = config.chasecaleb.username;
      };
      "aws-config/personal" = {
        owner = config.chasecaleb.username;
        path = "/home/${config.chasecaleb.username}/.aws/config";
      };
    });
  };

  systemd.tmpfiles.rules = [
    # So that I don't have to add "--flake foo" to nixos-rebuild
    "L+ /etc/nixos/flake.nix - - - - /home/caleb/code/nixos-config/flake.nix"
  ] ++ (if config.chasecaleb.workFeatures then [
    # Feature flag as a file
    "f /etc/chasecaleb-work - - - - -"
  ] else [ ]);

  virtualisation.docker = {
    enable = true;
  };
}
