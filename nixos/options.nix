{ lib, ... }:
{
  # Cross-cutting/general options (e.g. feature flags) go here.
  options = {
    chasecaleb.workFeatures = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
    # I can't think of a reason why I would ever want or need to change my main account username,
    # but it's nice having it as a value for the sake of tracking where it's used and so on.
    chasecaleb.username = lib.mkOption {
      type = lib.types.str;
      default = "caleb";
    };
  };
}
