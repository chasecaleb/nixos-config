# This MUST NOT be used inside a QEMU test VM (i.e. NixOS host install -> NixOS virt-manager VM),
# because the virbr0 interface causes extreme packet loss due to being on the same subnet as the
# main adapter. Yeah... that one took a bit to figure out.
{ lib, config, ... }: {
  # On first startup of virt-manager, there will be an error shown:
  # "Could not detect a default hypervisor"
  # To resolve, go to file -> add connection -> QEMU -> connect.
  virtualisation.libvirtd = {
    enable = true;
    # Don't automatically restart guests that were running on shutdown.
    onBoot = "ignore";
    # OVMF is UEFI firmware.
    qemu.ovmf.enable = true;
  };
  programs.dconf.enable = true; # Needed for virt-manager to save settings (e.g. connections).
}
