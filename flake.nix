{
  description = "My NixOS + Emacs system";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    homeManager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sopsNix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    myEmacs = {
      url = "gitlab:chasecaleb/emacs.d";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, homeManager, sopsNix, myEmacs, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
    rec {
      # To build against local Emacs, use "./with-emacs.sh foo" (directly in shell, not via nix)
      # instead of "nix foo".
      packages.${system} = {
        scripts = pkgs.stdenv.mkDerivation {
          name = "scripts";
          src = ./packaged-scripts;
          phases = "installPhase fixupPhase";
          installPhase = ''
            mkdir "$out"
            cp -R --no-target-directory "$src" "$out/bin"
          '';
        };
        wrappedEslintServer = pkgs.writeShellScriptBin "node-eslint-wrapper" ''
          server_path=share/vscode/extensions/dbaeumer.vscode-eslint/server/out/eslintServer.js
          "${pkgs.nodejs}/bin/node" "${pkgs.vscode-extensions.dbaeumer.vscode-eslint}/$server_path" "$@"
        '';
      };

      nixosConfigurations =
        let
          nixosConfigFor = { name, extraModules }: {
            ${name} = nixpkgs.lib.nixosSystem
              {
                system = "x86_64-linux";
                modules = extraModules ++ [
                  myEmacs.nixosModule.${system}
                  ./nixos/options.nix
                  ./nixos/base.nix
                  ./nixos/networking.nix
                  ./nixos/x11.nix
                  homeManager.nixosModules.home-manager
                  sopsNix.nixosModules.sops
                ] ++ [{
                  networking.hostName = name;
                  home-manager = {
                    # Gotta love the pkgs vs packages inconsistency.
                    useGlobalPkgs = true;
                    useUserPackages = true;
                    users.caleb = import ./home/home.nix;
                  };
                }];
                specialArgs.myPkgs = builtins.attrValues packages.${system};
                specialArgs.myEmacsPkgs = myEmacs.packages.${system};
              };
          };
        in
        (nixosConfigFor {
          name = "test";
          extraModules = [ ./nixos/test-vm.nix ];
        }) // (nixosConfigFor {
          name = "desktop";
          extraModules = [ ./nixos/vmware.nix ./nixos/vm-host.nix ];
        }) // (nixosConfigFor {
          name = "work-vmware";
          extraModules = [
            ./nixos/vmware.nix
            ./nixos/vm-host.nix
            { chasecaleb.workFeatures = true; }
          ];
        });

      devShell.${system} = pkgs.mkShell {
        packages = [
          pkgs.age
          pkgs.coreutils-full
          pkgs.dialog
          pkgs.git
          pkgs.jq
          pkgs.openssh
          pkgs.sops
          pkgs.terraform
        ];
      };
    };
}
