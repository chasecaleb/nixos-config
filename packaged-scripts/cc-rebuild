#!/usr/bin/env bash
# Convenience wrapper for nixos-rebuild to simplify working with my emacs.d repo.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

do_check=false
local_emacs=false
while true; do
    if [[ $# == 0 ]]; then
        # No more options, done parsing
        break
    fi
    case ${1:-} in
        --local)
            local_emacs=true
            ;;
        --check)
            do_check=true
            ;;
        --)
            # Rest of args will be passed to nix.
            shift
            break
            ;;
        *)
            die "Unknown argument: $1"
            ;;
    esac
    shift
done

cd ~/code/nixos-config
if [[ "$do_check" == "true" ]]; then
    nix flake check
fi

nix_options=()
if [[ "$local_emacs" == "true" ]]; then
    nix_options+=(--override-input myEmacs "path://$HOME/code/emacs.d")
else
    # Because remembering to keep the lock file updated is a pain.
    nix flake lock --update-input myEmacs
fi

sudo nixos-rebuild "${nix_options[@]}" "$@"
