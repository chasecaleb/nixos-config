#!/usr/bin/env bash
# Clone all starred GitLab repos.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

if [[ -f /etc/chasecaleb-work ]]; then
    token_path=work/glab/cchase
    user_id=7805406
    base_dir=$HOME/nipr
else
    token_path=tokens/glab/chasecaleb
    user_id=5613330
    base_dir=$HOME/code
fi
token=$(pass "$token_path")

if [[ ! -e "$base_dir" ]]; then
    mkdir -p "$base_dir"
fi

while IFS= read -r project; do
    path=$(jq -er '.path' <<< "$project")
    url=$(jq -er '.ssh_url_to_repo' <<< "$project")
    git clone "$url" "$base_dir/$path"
done < <(curl -H "authorization: Bearer $token" -fsSL \
    "https://gitlab.com/api/v4/users/$user_id/starred_projects?simple=true" \
    | jq -ecr '.[] | {path, ssh_url_to_repo}')
