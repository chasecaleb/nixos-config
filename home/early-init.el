;;; -*- lexical-binding: t -*-
;; Flycheck only works with files that are byte-compiled, so that makes byte-compiling important
;; regardless of performance motivations.
(require 'comp)
(setq native-comp-async-report-warnings-errors nil)

;; Native comp causes `org-offer-links-in-entry' to always pick first link from list if called from
;; start of a headline with multiple links. Sounds insane, but seriously.
;;
;; Proof I'm not the only one:
;; - https://debbugs.gnu.org/cgi/bugreport.cgi?bug=51382
;; - https://old.reddit.com/r/emacs/comments/qf7o8t/orgofferlinksinentry_is_incompatible_with/
(setq native-comp-deferred-compilation-deny-list (list (rx "/org.el" string-end)))
(setq warning-suppress-types '((comp)))

(setq package-enable-at-startup nil)
