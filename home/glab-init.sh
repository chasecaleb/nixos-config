#!/usr/bin/env bash
# Authentication setup for glab.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

if git remote -v | grep --quiet 'gitlab.com.*NAIC'; then
    name=work/glab/cchase
else
    name=tokens/glab/chasecaleb
fi
GITLAB_TOKEN=$(pass "$name")
export GITLAB_TOKEN
