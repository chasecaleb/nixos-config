;;; -*- lexical-binding: t -*-
;;; Shim to load my Emacs config from its Nix system package, since Emacs is rather stubborn about
;;; configuring the location of init.el.
;;; Fortunately thanks to Nix it's already in `load-path'.
(require 'cc-init)
