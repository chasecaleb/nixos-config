#!/usr/bin/env bash
# Generate new key for use when installing on a new machine.

# Reminder: to mount a shared path in a QEMU VM, the command is:
#   mkdir /share && mount -t 9p -o trans=virtio,version=9p2000.L /share /share

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

confirm_or_die() {
    PROMPT=$1
    local confirm
    read -re -p "$PROMPT [y/N] " confirm
    if [[ "$confirm" != "y" && "$confirm" != "Y" ]]; then
        die "Cancelled."
    fi
}

if [[ $# != 2 ]]; then
    die "Must provide two args: machine name and filename to save key to"
fi
name=$1
destination=$2

if [[ -e "$destination" ]]; then
    die "Key file already exists: $destination"
fi

script_dir=${BASH_SOURCE%/*}
sops_config=$(realpath "$script_dir/../.sops.yaml")
if [[ $(git status --porcelain "$script_dir/../secrets" "$sops_config") != "" ]]; then
    die "Uncommitted changes to secrets or SOPS config, refusing to run"
fi

if grep "&$name " "$sops_config"; then
    confirm_or_die "WARNING: Key for $name already exists in $sops_config. Replace it? "
fi

age-keygen -o "$destination"
chmod 400 "$destination"
public_key=$(age-keygen -y "$destination")

# I would use yq to manipulate yaml, but it doesn't preserve anchors and references. Fortunately
# this is trivial enough to do with sed.
# https://github.com/mikefarah/yq/issues/520

# Remove existing key entry for the machine, if there is one.
sed -e "/&$name /d" \
    -i "$sops_config"
# Add the new key.
sed -e "/^keys:/a \  - &$name $public_key" \
    -i "$sops_config"

"$script_dir/sops-refresh.sh"

echo "Created new key at $destination and added to SOPS."
echo "IMPORTANT: Make sure to commit and push changes to secrets files!"
