#!/usr/bin/env bash
# Don't run this directly. See bootstrap.sh instead.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

confirm_or_die() {
    PROMPT=$1
    local CONFIRM
    read -re -p "$PROMPT [y/N] " CONFIRM
    if [[ "$CONFIRM" != "y" && "$CONFIRM" != "Y" ]]; then
        die "Cancelled."
    fi
}

# Get user input for password with confirmation.
# Response will be stored in $REPLY.
# $1 = prompt string (trailing colon/space will be added).
read_pw() {
    local FIRST SECOND
    while true; do
        read -rs -p "$1: " FIRST
        echo
        read -rs -p "$1 (again): " SECOND
        echo

        if [[ "$FIRST" == "$SECOND" ]]; then
            break
        else
            echo "Inputs did not match, try again"
        fi
    done
    REPLY="$FIRST"
}

get_install_disk() {
    local disk_list
    disk_list=$(lsblk --nodeps --paths --list --noheadings --sort size --output name,size \
        | grep -Ev "boot|rpmb|loop" \
        | tac)
    # Intentionally expanding list so that dialog interprets it as menu items.
    # shellcheck disable=2086
    disk=$(dialog --stdout --menu "Select install disk" 0 0 0 $disk_list)
    clear
    echo "=== lsblk output ==="
    lsblk --noheadings --paths
    echo "===================="
    confirm_or_die "WARNING: You will lose all data on $disk. Continue?"
}

# Note regarding swap: instead of disk-based swap (e.g. partition or file) I'm using zram (which is
# effectively a compressed section of ram). Downside is no hibernation, but I wouldn't use it
# anyways since my machines have a lot of RAM (making hibernation slower than a reboot).
#
# This is also what Fedora started doing by default as of Fedora 33, see here (ctrl+f zram):
# https://docs.fedoraproject.org/en-US/fedora/f35/install-guide/install/Installing_Using_Anaconda/
do_partition() {
    # Make sure things aren't still mounted (if re-running installer without rebooting).
    umount --recursive /mnt || true
    dmsetup remove_all

    # Keep in mind that GPT partition labels (/dev/disk/by-partlabel) and
    # filesystem labels (/dev/disk/by-label) are two different things.
    # https://wiki.archlinux.org/index.php/Persistent_block_device_naming
    sgdisk --zap-all "$disk"
    sgdisk --clear \
        --new=1:0:+550MiB --typecode=1:ef00 --change-name=1:EFI \
        --new=2:0:0 --typecode=2:8300 --change-name=2:cryptsystem \
        "$disk"
    partprobe

    # Need --force to make this work if re-run a second time without rebooting.
    wipefs --all --force "$partition_efi" "$partition_luks"
    mkfs.fat -F32 -n EFI "$partition_efi"
    # echo -n to prevent trailing newline from becoming part of password.
    echo -n "$disk_pw" | cryptsetup luksFormat --type luks2 "$partition_luks" -
    echo -n "$disk_pw" | cryptsetup open "$partition_luks" system
    mkfs.ext4 -L system /dev/mapper/system

    mount /dev/mapper/system /mnt
    mkdir /mnt/boot
    mount "$partition_efi" /mnt/boot
}

if [[ "$UID" != 0 ]]; then
    die "Must run as root"
fi

partition_efi=/dev/disk/by-partlabel/EFI
partition_luks=/dev/disk/by-partlabel/cryptsystem

script_dir=${BASH_SOURCE%/*}
cd "$script_dir/../"

read_pw "Disk encryption password" && disk_pw="$REPLY" && unset REPLY
get_install_disk
do_partition
