#!/usr/bin/env bash
# Wrapper for sops to provide access to key file.
#
# USAGE: Run this within nix develop (which should happen automatically via direnv within this
# project).

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# Kludge to temporarily make root-owned key file visible to user so that I can edit it in my Emacs
# session. It might seem more to just do this as root with e.g. nano, but if my user account is
# compromised then the game is already over since the attacker could install an X11 keylogger and
# steal my sudo password (or do any of a million other things, like manipulate this script).
# Based on https://stackoverflow.com/a/68901504
key_copy=$(mktemp)
exec {fd}<>"$key_copy"
rm -f "$key_copy"
unset key_copy # Make sure I don't actually re-create the /tmp file by using this variable.
export SOPS_AGE_KEY_FILE=/dev/fd/$fd
# shellcheck disable=SC2024
sudo cat /var/lib/private/sops.key > "$SOPS_AGE_KEY_FILE"

sops "$@"
