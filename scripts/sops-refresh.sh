#!/usr/bin/env bash
# Re-key and re-encrypt SOPS secrets.
#
# SOPS has a minor footgun because two separate commands are needed, see:
# https://github.com/mozilla/sops/issues/365

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

script_dir=${BASH_SOURCE%/*}
cd "$script_dir/../"

while IFS= read -rd '' file; do
    echo "Refreshing encryption for: $file"
    "$script_dir/do-sops.sh" updatekeys --yes "$file"
    "$script_dir/do-sops.sh" --rotate --in-place "$file"
done < <(find ./secrets -type f -print0 | sort -z)
