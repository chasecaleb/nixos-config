#!/usr/bin/env bash
# USAGE:
#   1. Run bootstrap-key.sh on an existing machine to generate a new SOPS key.
#   2. Boot the NixOS live ISO.
#   3. Transfer the generated SOPS private key (via e.g. USB drive or SSH).
#   4. Run this script: sudo bash -c "$(curl -fsSL https://bit.ly/3sQr6hP)"

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

confirm_or_die() {
    PROMPT=$1
    local confirm
    read -re -p "$PROMPT [y/N] " confirm
    if [[ "$confirm" != "y" && "$confirm" != "Y" ]]; then
        die "Cancelled."
    fi
}

# Can't modify /etc/nix/nix.conf on the live ISO, so this will have to do instead.
do_nix() {
    nix --extra-experimental-features nix-command --extra-experimental-features flakes "$@"
}

get_nixos_machine() {
    local machine_list=()
    local i=0
    while IFS= read -r line; do
        i=$((i + 1))
        machine_list+=("$line" "")
    done < <(do_nix eval '.#nixosConfigurations' \
        --apply 'builtins.attrNames' --json \
        | jq --raw-output --exit-status '.[]')
    machine_name=$(dialog --stdout --menu "Select NixOS configuration" 0 0 0 "${machine_list[@]}")
    clear
}

if [[ $UID != 0 ]]; then
    # So technically most of this script could run as a normal user and then elevate a couple parts
    # (e.g. nixos-install) with sudo, but considering this is running in a live ISO there's no harm
    # in running the whole thing as root.
    die "This script must be run as root"
fi

read -re -p "SOPS key file: " sops_key_file_input
if [[ ! -e "$sops_key_file_input" ]]; then
    die "Key file does not exist: $sops_key_file_input"
elif ! grep --quiet "AGE-SECRET-KEY" "$sops_key_file_input"; then
    die "Expected \$1 to be an AGE private key for SOPS: $sops_key_file_input"
fi
# Potentially need key on ISO for activation script.
sops_key_file_iso=/var/lib/private/sops.key
mkdir -p "$(dirname "$sops_key_file_iso")"
cp "$sops_key_file_input" "$sops_key_file_iso"

# NixOS 21.11 live ISO doesn't have "nix develop" and flakes support, irritatingly.
# Also need git, for obvious reasons.
nix-env -iA nixos.nixUnstable nixos.git nixos.jq nixos.dialog

repo_dir=/tmp/nixos-config
if [[ -e "$repo_dir" ]]; then
    rm -rf "$repo_dir"
fi
git clone https://gitlab.com/chasecaleb/nixos-config "$repo_dir"
cd "$repo_dir"
get_nixos_machine
./scripts/bootstrap-hardware.sh

sops_key_destination=/mnt/$sops_key_file_iso
# Transfer SOPS key to the installed system.
mkdir -p "$(dirname "$sops_key_destination")"
cp "$sops_key_file_iso" "$sops_key_destination"

# Bound mount is an ugly hack to work around a nixos-install bug. Maybe it will be fixed by 2030
# with any luck?
# https://github.com/NixOS/nixpkgs/issues/73404
mkdir /mnt/mnt || true
mount --bind /mnt /mnt/mnt

# No root password to avoid prompt at end of install.
nixos-install --no-root-password --root /mnt --flake ".#${machine_name}"
