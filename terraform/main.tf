terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.3.0"
    }
  }

  required_version = ">= 1.1.0"

  backend "s3" {
    bucket = "tf-state-nixos"
    key    = "state/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
  default_tags {
    tags = {
      managed-by = "terraform"
      project    = "nixos"
    }
  }
}

resource "aws_s3_bucket" "terraform-state" {
  bucket = "tf-state-nixos"
}

resource "aws_s3_bucket_versioning" "terraform-state-versioning" {
  bucket = aws_s3_bucket.terraform-state.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket" "nix-cache" {
  bucket = "chasecaleb-nix-cache"
}

resource "aws_iam_user" "nix-daemon" {
  name = "nix-daemon"
}

# To access this:
# terraform state pull | jq -r '.resources[] | select(.type == "aws_iam_access_key") | .instances[0].attributes.secret'
resource "aws_iam_access_key" "nix-daemon" {
  user = aws_iam_user.nix-daemon.name
}

resource "aws_iam_user_policy" "nix-daemon" {
  name = "nix-daemon"
  user = aws_iam_user.nix-daemon.name
  policy = jsonencode(
    {
      Version : "2012-10-17"
      Statement : [
        {
          Effect : "Allow"
          Action : [
            "s3:AbortMultipartUpload",
            "s3:GetBucketLocation",
            "s3:GetObject",
            "s3:ListBucket",
            "s3:ListBucketMultipartUploads",
            "s3:ListMultipartUploadParts",
            "s3:PutObject"
          ],
          Resource : [
            aws_s3_bucket.nix-cache.arn,
            "${aws_s3_bucket.nix-cache.arn}/*"
          ]
        }
      ]
  })
}
